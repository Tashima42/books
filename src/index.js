const util = require('util')
const fs = require('fs')
const DIRNAME = __dirname
const BASEDIR = `${DIRNAME}/../books`

const downloadBaseUrl = 'https://gitlab.com/Tashima42/books/-/blob/main/books'

function main() {
  const json = {}
  json.languages = []

  const languages = fs.readdirSync(BASEDIR)
  languages.forEach((language, languageIndex) => {
    json.languages.push({ language: language, authors: [] })

    const authorsDir = `${BASEDIR}/${language}`
    const authors = fs.readdirSync(authorsDir)

    authors.forEach((author, authorIndex) => {
      json.languages[languageIndex].authors.push({ name: author, books: [] })

      const booksdir = `${authorsDir}/${author}`
      const books = fs.readdirSync(booksdir)

      books.forEach((book, bookIndex) => {
        json.languages[languageIndex].authors[authorIndex].books.push({
          name: book,
          cover: "https://images-na.ssl-images-amazon.com/images/I/51C0FqUk9FL._SX325_BO1,204,203,200_.jpg",
          links: {}
        })

        const bookDir = `${booksdir}/${book}`
        const bookFiles = fs.readdirSync(bookDir)

        // pt/Alexandre%20Herculano/Eurico%20o%20Presbitero/Eurico-o-Presbitero%5BAlexandre-Herculano%5D.mobi

        const types = bookFiles.map(fileName => {
          const type = fileName.split('.')[1]
          json.languages[languageIndex].authors[authorIndex]
            .books[bookIndex].links[type] = generateUrl({
              baseUrl: downloadBaseUrl,
              language,
              author,
              book,
              type,
              fileName
            })
        })
      })
    })
  })
  return json
}

function generateUrl({ baseUrl, language, author, book, fileName }) {
  language = encodeURIComponent(language)
  author = encodeURIComponent(author)
  book = encodeURIComponent(book)
  fileName = encodeURIComponent(fileName)

  return `${baseUrl}/${language}/${author}/${book}/${fileName}`
}


const j = main()
console.log(util.inspect((j), false, null, true))

fs.writeFileSync('bookList.json', JSON.stringify(j))
